import React from 'react'


const Form = () => {
  return (
    <form className="row container mb-5 mt-5">
        <div className="form-group col-md-8">
            <input type="text" name="tech" className="form-control form-control-lg" placeholder="Enter data ..." />
        </div>
        <button className="btn btn-primary col-md-4">Add</button>
    </form>
  )
}

export default Form;