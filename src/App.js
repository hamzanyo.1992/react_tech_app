import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Form from "./components/Form";
import Listing from "./components/Listing";

class App extends Component {
  state = {
    courses : [
      {name : "HTML5"},
      {name : "CSS3"},
      {name : "PHP"}
    ]
  }


  render() {
    const {courses} = this.state;
    const courseList = courses.map((course, index) =>{
      return <Listing key={index} details={course} />
    })
    return (
      <div className="App container">
        <h1>Tech App</h1>
        <Form />
        <ul>{courseList}</ul>
      </div>
    );
  }
}

export default App;
